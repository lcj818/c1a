import cv2

img = cv2.imread(r'.\Lena.png')

# 题目一，显示原图
cv2.imshow('Hello, world!', img)

gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# 题目二，原图
cv2.namedWindow("source image", 0)
cv2.resizeWindow("source image", 640, 480)
cv2.imshow("source image", img)

# 题目二，原图
cv2.namedWindow("gray", 0)
cv2.resizeWindow("gray", 640, 480)
cv2.imshow("gray", gray)

hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# 题目二，H分量
cv2.namedWindow("Hue", 0)
cv2.resizeWindow("Hue", 640, 480)
cv2.imshow("Hue", hsv[:, :, 0])

# 题目二，S分量
cv2.namedWindow("Saturation", 0)
cv2.resizeWindow("Saturation", 640, 480)
cv2.imshow("Saturation", hsv[:, :, 1])

# 题目二，V分量
cv2.namedWindow("Value", 0)
cv2.resizeWindow("Value", 640, 480)
cv2.imshow("Value", hsv[:, :, 2])

# 题目二，蓝色分量
cv2.namedWindow("Blue", 0)
cv2.resizeWindow("Blue", 640, 480)
cv2.imshow("Blue", img[:, :, 0])

# 题目二，绿色分量
cv2.namedWindow("Green", 0)
cv2.resizeWindow("Green", 640, 480)
cv2.imshow("Green", img[:, :, 1])

# 题目二，红色分量
cv2.namedWindow("Red", 0)
cv2.resizeWindow("Red", 640, 480)
cv2.imshow("Red", img[:, :, 2])

cv2.waitKey()
cv2.destroyAllWindows()
